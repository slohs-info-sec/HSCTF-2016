from string import whitespace
# removes whitespace including tabs, line breaks, and spaces
def remove_whitespace(x):
    for c in whitespace:
        x = x.replace(c, "")
    return x

# converts the raw code string into a list of rules, each of which is in the
# form [lhs, rhs]
def string_to_code(raw_code):
    try:
        if raw_code[-1] != ";":
            return None
        return [i.split("=") for i in remove_whitespace(raw_code[:-1]).split(";")]
    except:
        return None

# run the actual program with given rules, an initial state
# the correct output that is desired, the maximum number of
# steps the computation should be run for, and the maximum size of
# the state string
def run_program(rules, input_state, correct_output, max_steps, max_size):
    try:
        if rules == None:
            return {"correct":False, "error":"Program threw an error"}
        state = remove_whitespace(input_state)
        if len(state) > max_size:
            return {"correct":False, "error":"Size limit exceeded"}

        for i in range(max_steps):
            rule_found = False
            for [k, v] in rules:
                if k in state:
                    state = state.replace(k, v, 1)
                    rule_found = True
                    break

            if len(state) > max_size:
                return {"correct":False, "error":"Size limit exceeded"}

            if not rule_found:
                if state == correct_output:
                    return {"correct":True, "error":None}
                else:
                    return {"correct":False, "error":"Incorrect return value"}

        return {"correct":False, "error":"Did not terminate in time"}
    except:
	    return {"correct":False, "error":"Program threw an error"}
