/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package keithandthebigbadsequence;

import java.io.*;
import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author Ganden
 */
public class KeithAndTheBigBadSequence {

    static BigInteger[] array = new BigInteger[10001];

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            assignArray();
        } catch (FileNotFoundException ex) {

        }
        System.out.println(isEven(chooseRec(array[3], new BigInteger("952"))));

//        int longestSoFar = 0;
//        int smallestPossibleStartingIndex = 0;
//        for (int start = 0; start < array.length; start++) {
//            System.out.println("Started outer loop for " + start);
//            for (int end = start; end < array.length; end++) {
//                System.out.println("\tStarted inner loop for " + end);
//                for (int i = end; i >= start; i--) {
//                    if (!isEven(chooseRec(array[i], new BigInteger(String.valueOf(end - start + 1))))) {
//                        break;
//                    }
//                    // there was no break so all values in the current subsequence choose
//                    // the subsequence length are valid
//                    if (end - start + 1 > longestSoFar) {
//                        longestSoFar = i - start + 1;
//                        smallestPossibleStartingIndex = start;
//                        System.out.println("\t\tlongestSoFar = " + longestSoFar);
//                    }
//                }
//            }
//        }
//        System.out.println(longestSoFar + " " + smallestPossibleStartingIndex);
    }

    static void assignArray() throws FileNotFoundException {
        Scanner rd = new Scanner(new File("../bigbad.in"));
        int count = 0;
        while (rd.hasNext()) {
            array[count] = rd.nextBigInteger();
            count++;
        }
    }

//    static BigInteger choose(BigInteger n, BigInteger k) {
//        return factorial(n).divide(factorial(k).multiply(factorial(n.subtract(k))));
//    }
    static BigInteger chooseRec(BigInteger n, BigInteger k) {
        // WORKING ON CONVERTING THE BELOW LINES INTO A ITERATIVE VERSION. THEN DELETE THE FACTORIAL METHOD.
        if (k.equals(BigInteger.ZERO)) {
            return BigInteger.ONE;
        }
        return (n.multiply(chooseRec(n.subtract(BigInteger.ONE), k.subtract(BigInteger.ONE)))).divide(k);
    }

    static BigInteger chooseLoop(BigInteger n, BigInteger k) {
        BigInteger value = BigInteger.ONE;
        int timesToDivide = 0;
        while (!k.equals(BigInteger.ZERO)) {
            value = value.multiply(n);
            n = n.subtract(BigInteger.ONE);
            k = k.subtract(BigInteger.ONE);
            timesToDivide++;
        }
        for (int i = 0; i < timesToDivide; i++) {
            value = value.divide(k);
            k = k.add(BigInteger.ONE);
        }
        return value;
    }

    static BigInteger choose3(BigInteger n, BigInteger k) {
        BigInteger ret = BigInteger.ONE;
        for (int i = 0; BigInteger.ONE.compareTo(k) < 0; i++) {
            ret = ret.multiply(n.subtract(BigInteger.valueOf(i)))
                    .divide(BigInteger.valueOf(i + 1));
        }
        return ret;
    }

    static BigInteger factorial(BigInteger n) {
        System.out.println("\t\t\t\tcalcing factorial");
        BigInteger fact = new BigInteger("1");
        for (int i = 1; new BigInteger(String.valueOf(i)).compareTo(n) <= 0; i++) {
            fact = fact.multiply(new BigInteger(String.valueOf(i)));
        }
        System.out.println("\t\t\t\tfinished calcing fact");
        return fact;
    }

    public static boolean isEven(BigInteger number) {
        return number.getLowestSetBit() != 0;
    }

}
