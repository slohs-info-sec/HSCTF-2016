/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funnyfactorials;

import java.math.*;
import org.apache.commons.math3.fraction.*;
import java.io.*;
import java.text.*;

/**
 *
 * @author Ganden
 */
public class FunnyFactorials {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BigFraction output = new BigFraction(0);
        double x = 0.7893;

        for (int j = 2; j <= 5000; j++) {
            BigInteger factOfJ = factorial(new BigInteger(String.valueOf(j)));
            //System.out.println("factorial of " + j + " is " + factOfJ);
            BigFraction coef = new BigFraction(factOfJ.subtract(new BigInteger("1")).multiply(new BigInteger("2")), factOfJ);
            //System.out.println(coef);
            double xPartDbl = Math.pow(x, j);
            System.out.println("xPartDbl = " + xPartDbl);
            DecimalFormat df = new DecimalFormat();
            String xPartStr = String.valueOf(xPartDbl);
            if(xPartStr.indexOf('E') > 0) {
                df.setMaximumFractionDigits(Integer.parseInt(xPartStr.substring(
                        xPartStr.indexOf('E') + 2)) + xPartStr.length());
            } else {
                df.setMaximumFractionDigits(xPartStr.length());
            }
            xPartStr = df.format(xPartDbl);
            System.out.println("xPartStr = " + xPartStr);
            int placesAfterDecimal = xPartStr.length() - 1 - xPartStr.indexOf('.');
            System.out.println("placesAfterDecimal = " + placesAfterDecimal);
            BigFraction xPart = new BigFraction(
                    new BigInteger(xPartStr.substring(2)),
                    new BigInteger("10").pow(placesAfterDecimal));
            System.out.println("xPartUnreduced = " + new BigInteger(xPartStr.substring(2))
                    + " / " + new BigInteger("10").pow(placesAfterDecimal));
            System.out.println("xPart = " + xPart);
            output = output.add(coef.multiply(xPart));
            //System.out.println();
            System.out.println("Finished adding term for j = " + j);
        }
        
        System.out.println(output);
        System.out.println(output.doubleValue());
        
        try (PrintWriter writer = new PrintWriter("output.txt", "UTF-8")) {
            writer.println(output);
            writer.println(output.doubleValue());
            writer.close();
        } catch(Exception e) {
            
        }
    }

    static BigInteger factorial(BigInteger number) {
        if (number.compareTo(new BigInteger("1")) <= 0) {
            return new BigInteger("1");
        } else {
            return number.multiply(factorial(number.subtract(new BigInteger("1"))));
        }
    }

}
