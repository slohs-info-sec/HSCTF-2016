/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wondrousnumbers;

/**
 *
 * @author Ganden
 */
public class WondrousNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int mostSteps = 0;
        int highest = 0;
        for (int i = 1; i <= 100; i++) {
            System.out.println("i is " + i);
            int steps = 0;
            int j = i;
            while (j != 1) {
                if (j % 2 == 0) {
                    j /= 2;
                } else {
                    j = 3 * j + 1;
                }
                steps++;
            }
            if (steps > mostSteps) {
                mostSteps = steps;
                highest = i;
            }
        }
        System.out.println(highest);
    }

}
