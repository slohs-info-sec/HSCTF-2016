/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package keithandtheegyptiantomb;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Ganden
 */
public class KeithAndTheEgyptianTomb {

    private static int[][] array = new int[10000][10000];
    private static int[][] multTable = new int[10000][10000];

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        try {
//            assignArray();
//        } catch (FileNotFoundException e) {
//            System.out.println(e);
//        }

        assignMultTable();
        printArray(multTable);
    }

    static void assignArray() throws FileNotFoundException {
        Scanner reader = new Scanner(new File("../egypt.in"));
        int row = 0;
        int col = 0;
        while (reader.hasNext()) {
            array[row][col] = reader.nextInt();
            row++;
            if (row == array.length) {
                System.out.println(col);
                row = 0;
                col++;
            }
        }
    }

    static void assignMultTable() {
        for (int startRow = 0; startRow < multTable.length; startRow++) {
            for (int startCol = 0; startCol < multTable[0].length; startCol++) {
                for (int size = 1; size <= multTable.length - startRow && size <= multTable[0].length - startCol; size++) {
                    for (int row = startRow; row < startRow + size; row++) {
                        for (int col = startCol; col < startCol + size; col++) {
                            multTable[row][col]++;
                        }
                    }
                }
            }
        }
    }

    static void printArray(int[][] a) {
        try (java.io.PrintWriter writer = new java.io.PrintWriter("output/" + a.length + ".txt", "UTF-8")) {
            writer.println(a.length + " x " + a[0].length + "\n");
            for (int[] row : a) {
                for (int col : row) {
                    writer.print(col + " ");
                }
                writer.println();
            }
            writer.close();
        } catch (Exception e) {

        }
    }

}
