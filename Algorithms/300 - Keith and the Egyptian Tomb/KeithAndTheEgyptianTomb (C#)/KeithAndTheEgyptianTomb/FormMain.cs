﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeithAndTheEgyptianTomb
{
    public partial class FormMain : Form
    {
        private static int[,] multTable;

        public FormMain()
        {
            InitializeComponent();
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            try
            {
                multTable = new int[int.Parse(txtSize.Text), int.Parse(txtSize.Text)];
                prgStartRow.Maximum = int.Parse(txtSize.Text);
                assignMultTable();
                printArray(multTable);
            }
            catch (Exception ex)
            {

            }
        }

        private void assignMultTable()
        {
            for (int startRow = 0; startRow < multTable.GetLength(0); startRow++)
            {
                prgStartRow.Value = startRow + 1;
                for (int startCol = 0; startCol < multTable.GetLength(1); startCol++)
                {
                    for (int size = 1; size <= multTable.GetLength(0) - startRow && size <= multTable.GetLength(1) - startCol; size++)
                    {
                        for (int row = startRow; row < startRow + size; row++)
                        {
                            for (int col = startCol; col < startCol + size; col++)
                            {
                                multTable[row, col]++;
                            }
                        }
                    }
                }
            }
        }

        private void printArray(int[,] a)
        {
            string text = "";
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    text += a[i, j] + " ";
                }
                text += "\n";
            }
            File.WriteAllText(
                a.GetLength(0) + ".txt",
                text);
        }

        private void FormMain_Resize(object sender, EventArgs e)
        {
            prgStartRow.Width = this.Width - 40;
        }
    }
}
