/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hadhadhad;

import java.awt.Point;
import java.math.BigInteger;

/**
 *
 * @author Ganden
 */
public class HadHadHad {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Point p = new Point(0, 100);

        BigInteger[][] hads = new BigInteger[2][p.y + 1]; // hads[0] = incorrect, hads[1] = correct
        // also, we ignore hads[i][0] for ease of access
        hads[0][1] = new BigInteger("1");
        hads[1][1] = new BigInteger("2");
        for (int i = 2; i < hads[0].length; i++) {
            hads[0][i] = hads[1][i - 1].add(
                    hads[0][i - 1].multiply(new BigInteger("2"))).add(
                    new BigInteger("6"));
            hads[1][i] = hads[0][i - 1].add(
                    hads[1][i - 1].multiply(new BigInteger("2"))).add(
                    new BigInteger("6"));
        }

        System.out.println("'had's in the " + p.y + "th " + (p.x == 0 ? "in" : "")
                + "correct iteration: " + hads[p.x][p.y]);
    }

    // recursive way below (2 methods), worked for low numbers but took AGES for large ones
    static BigInteger incorrect(int iteration) {
        if (iteration == 1) {
            return new BigInteger("1");
        } else {
            if (iteration > 10) {
                System.out.println("i(" + iteration + ")");
            }
            return correct(iteration - 1).add(
                    incorrect(iteration - 1).multiply(new BigInteger("2"))).add(
                    new BigInteger("6"));
        }
    }

    static BigInteger correct(int iteration) {
        if (iteration == 1) {
            return new BigInteger("2");
        } else {
            if (iteration > 10) {
                System.out.println("c(" + iteration + ")");
            }
            return incorrect(iteration - 1).add(
                    correct(iteration - 1).multiply(new BigInteger("2"))).add(
                    new BigInteger("6"));
        }
    }
}
