/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package permutednumbers;

/**
 *
 * @author Ganden
 */
public class PermutedNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] table = new int[]{-1, 2, 4, 8, 16, 3, 6, 12, 24, 19, 9, 18, 7, 14, 28, 27, 25, 21, 13, 26, 23, 17, 5, 10, 20, 11, 22, 15, 1};
        int[] input = new int[]{23, 24, 2, 23, 1, 10, 2, 26, 28, 23, 1, 5, 3, 13, 11, 1, 24, 2, 13, 16, 1, 23, 27, 1, 4, 13, 3, 2, 18, 1, 2, 6, 23, 3, 13, 1, 2, 7, 7};
        
        String out = "";
        for(int encoded : input) {
            for(int decoded = 0; decoded < table.length; decoded++) {
                if(encoded == table[decoded]) {
                    out += intToLetter(decoded);
                    break;
                }
            }
        }
        System.out.println(out);
    }
    
    static char intToLetter(int in) {
        if(1 <= in && in <= 26) {
            return (char)(in + 96);
        } else if(in == 27) {
            return ' ';
        } else { // in == 28
            return '_';
        }
    }
}
