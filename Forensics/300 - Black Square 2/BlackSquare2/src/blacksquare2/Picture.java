/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blacksquare2;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * A class that represents a picture. This class inherits from SimplePicture and
 * allows the student to add functionality to the Picture class.
 *
 * @author Barbara Ericson ericson@cc.gatech.edu
 */
public class Picture extends SimplePicture {
    ///////////////////// constructors //////////////////////////////////

    /**
     * Constructor that takes no arguments
     */
    public Picture() {
        /* not needed but use it to show students the implicit call to super()
         * child constructors always call a parent constructor 
         */
        super();
    }

    /**
     * Constructor that takes a file name and creates the picture
     *
     * @param fileName the name of the file to create the picture from
     */
    public Picture(String fileName) {
        // let the parent class handle this fileName
        super(fileName);
    }

    /**
     * Constructor that takes the width and height
     *
     * @param height the height of the desired picture
     * @param width the width of the desired picture
     */
    public Picture(int height, int width) {
        // let the parent class handle this width and height
        super(width, height);
    }

    /**
     * Constructor that takes the width, height, and a color
     *
     * @param height the height of the desired picture
     * @param width the width of the desired picture
     * @param c the color of the desired picture
     */
    public Picture(int height, int width, Color c) {
        this(height, width);

        Pixel[][] pixels = this.getPixels2D();
        for (Pixel[] row : pixels) {
            for (Pixel elem : row) {
                elem.setColor(c);
            }
        }
    }

    /**
     * Constructor that takes a picture and creates a copy of that picture
     *
     * @param copyPicture the picture to copy
     */
    public Picture(Picture copyPicture) {
        // let the parent class do the copy
        super(copyPicture);
    }

    /**
     * Constructor that takes a buffered image
     *
     * @param image the buffered image to use
     */
    public Picture(BufferedImage image) {
        super(image);
    }

    ////////////////////// methods ///////////////////////////////////////
    /**
     * Method to return a string with information about this picture.
     *
     * @return a string with information about the picture such as fileName,
     * height and width.
     */
    @Override
    public String toString() {
        String output = "Picture, filename " + getFileName()
                + " height " + getHeight()
                + " width " + getWidth();
        return output;
    }

    /**
     * Method to set the blue to 0
     */

    /* Main method for testing - each class in Java can have a main 
     * method 
     */
    public static void main(String[] args) {
        Picture dankPic = new Picture("BlackSquare2_1.png");
        Pixel[][] pixels = dankPic.getPixels2D();
        boolean[][] bitArray = new boolean[pixels.length][pixels[0].length];
        for (int r = 0; r < pixels.length; r++) {
            for (int c = 0; c < pixels[r].length; c++) {
                if (pixels[r][c].getRed() == 122) { // dark pixel
                    bitArray[r][c] = true;
                } else if (pixels[r][c].getRed() == 198) { // light pixel
                    bitArray[r][c] = false;
                }
            }
        }
        //printArray(bitArray);
        String[] binaryArray = toBinaryStringArray(bitArray);
        String[] hexArray = new String[binaryArray.length];
        //printArray(binaryArray);
        for (int i = 0; i < hexArray.length; i++) {
            hexArray[i] = bin2Hex(binaryArray[i]);
        }
        //printArray(hexArray);

        try (java.io.PrintWriter writer = new java.io.PrintWriter("outputCHANGEME.txt", "UTF-8")) {
            for (boolean[] row : bitArray) {
                for (boolean col : row) {
                    writer.print(Boolean.toString(col).charAt(0));
                }
                writer.println();
            }
            writer.println();
            for (String elem : binaryArray) {
                writer.println(elem);
            }
            writer.println();
            for (String elem : hexArray) {
                writer.println(elem);
            }
            writer.println();
            for(String elem : hexArray) {
                writer.print(elem);
            }
            writer.println();
            writer.close();
        } catch (Exception e) {

        }
    }

    public static String[] toBinaryStringArray(boolean[][] a) {
        String[] toReturn = new String[a.length];
        for (int r = 0; r < a.length; r++) {
            String row = "";
            for (int c = 0; c < a[r].length; c++) {
                row += (a[r][c]) ? "1" : "0";
            }
            toReturn[r] = row;
        }
        return toReturn;
    }

    public static String bin2Hex(String number) {
        String hexValue = "";
        while (number.length() % 4 != 0) {
            number = "0" + number;
        }
        for (int i = number.length() / 4; i > 0; i--) {
            if (number.substring((i - 1) * 4, i * 4).equals("0000")) {
                hexValue = "0" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("0001")) {
                hexValue = "1" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("0010")) {
                hexValue = "2" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("0011")) {
                hexValue = "3" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("0100")) {
                hexValue = "4" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("0101")) {
                hexValue = "5" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("0110")) {
                hexValue = "6" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("0111")) {
                hexValue = "7" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("1000")) {
                hexValue = "8" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("1001")) {
                hexValue = "9" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("1010")) {
                hexValue = "A" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("1011")) {
                hexValue = "B" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("1100")) {
                hexValue = "C" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("1101")) {
                hexValue = "D" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("1110")) {
                hexValue = "E" + hexValue;
            } else if (number.substring((i - 1) * 4, i * 4).equals("1111")) {
                hexValue = "F" + hexValue;
            }
        }
        return hexValue;
    }

    private static void printArray(boolean[][] a) {
        for (boolean[] row : a) {
            for (boolean col : row) {
                System.out.println(Boolean.toString(col).charAt(0) + " ");
            }
            System.out.println();
        }
    }

    private static void printArray(String[] a) {
        for (String elem : a) {
            System.out.print(elem);
        }
    }

}
